from django.core.serializers import serialize
from django.conf import settings
from django.db import models
import json # Segunda forma de serializer
# Create your models here.

def upload_invoice(instance, filename):
    return "registers/{user}/{filename}".format(user=instance.user, filename=filename)

class RegisterQuerySet(models.QuerySet):
    """
    Esta forma es probada con la primera manera probada y la segunda manera probada 
    del serialize en Register
    def serialize(self):
        qs = self
        return serialize('json', qs, fields=('user', 'number', 'client', 'description', 'tax_rates', 'total_invoice', 'base'))
    """
    """
    def serialize(self):
        list_values = list(self.values('user', 'number', 'client', 'description', 'tax_rates', 'total_invoice', 'base'))
        print(list_values)
        final_array = []
        for obj in qs:
            stuct = json,loads(obj.serialize())
            final_array.apennd(stuct)
        return json.dumps(final_array)
    """
    def serialize(self): #No veoladiferencioa con este
        list_values = list(self.values("user", "number", "client", "description", "tax_rates", "total_invoice", "base"))
        return json.dumps(final_array)

class RegisterManager(models.Manager):
    def get_queryset(self):
        return RegisterQuerySet(self.model, using=self._db)

class Register(models.Model):
    # User in system
    user            = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete = models.CASCADE)
    # Register number of the invoice
    number          = models.IntegerField()
    # Invice description 
    description     = models.CharField(max_length=150)
    # Invoice image
    invoice_image   = models.ImageField(upload_to=upload_invoice, blank=True, null=True)
    # Updated invoice
    updated         = models.DateTimeField(auto_now=True)
    # Time stamp
    timestamp       = models.DateTimeField(auto_now_add=True)
    # Client name
    client          = models.CharField(max_length=150)
    # Register tax rate
    tax_rates       = models.DecimalField(max_digits=4, decimal_places=2)
    # Register total invoice
    total_invoice   = models.DecimalField(max_digits=20, decimal_places=2)
    # Register base
    base            = models.DecimalField(max_digits=20, decimal_places=2)

    def __str__(self):
        return self.description or "" 

    """
    Primera manera probada
    def serialize(self):
        return serialize("json", [self], fields=('user', 'number', 'client', 'description', 'tax_rates', 'total_invoice', 'base'))
    """    

    """
    Segunda manera probada diccionario
    """
    def serialize(self): # Este esta se muestra en SerilaizeDetailView
        json_data = serialize("json", [self], fields=('user', 'number', 'client', 'description', 'tax_rates', 'total_invoice', 'base'))
        stuct = json.loads(json_data)
        print(stuct)
        data = json.dumps(stuct[0]['fields'])
        return data